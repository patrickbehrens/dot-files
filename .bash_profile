
# iterm shell integration
source ~/.iterm2_shell_integration.`basename $SHELL`


alias sbp='source ~/.bash_profile && echo ~/.bash_profile loaded'
alias ba="code ~/.bash_aliases"
alias bp='code ~/.bash_profile && sbp'
alias pa='code ~/.personal_aliases && sbp'

# Homebrew paths
export PATH="/usr/local/sbin:~/bin:/usr/local/bin:$PATH"

###########################################
################ Homebrew Python ##########
###########################################
export PATH="/usr/local/opt/python/libexec/bin:$PATH"
source /usr/local/bin/virtualenvwrapper.sh


# if things get jammed up use these
# alias pip="pip2"
# alias python="python2"

# virtualenv
export WORKON_HOME=~/virtualenvxs
export VIRTUALENVWRAPPER_VIRTUALENV="/usr/local/bin/virtualenv"
export VIRTUALENVWRAPPER_PYTHON="/usr/local/opt/python/libexec/bin/python"

source /usr/local/bin/virtualenvwrapper.sh
test -e "${HOME}/.iterm2_shell_integration.bash" && source "${HOME}/.iterm2_shell_integration.bash"


###########################################
########### React Native Android ##########
###########################################
export ANDROID_HOME=$HOME/Library/Android/sdk
export PATH=$PATH:$ANDROID_HOME/tools
export PATH=$PATH:$ANDROID_HOME/platform-tools


###########################################
####### Start MEGADOTFILE LOADER ##########
###########################################

# Add `~/bin` to the `$PATH`
export PATH="$HOME/bin:$PATH";

# Load the shell dotfiles, and then some:
# * ~/.path can be used to extend `$PATH`.
# * ~/.extra can be used for other settings you don’t want to commit.
for file in ~/.{path,bash_prompt,exports,bash_aliases,functions,extra,personal_aliases}; do
    [ -r "$file" ] && [ -f "$file" ] && source "$file";
done;
unset file;

# Case-insensitive globbing (used in pathname expansion)
shopt -s nocaseglob;

# Append to the Bash history file, rather than overwriting it
shopt -s histappend;

# Autocorrect typos in path names when using `cd`
shopt -s cdspell;

# Enable some Bash 4 features when possible:
# * `autocd`, e.g. `**/qux` will enter `./foo/bar/baz/qux`
# * Recursive globbing, e.g. `echo **/*.txt`
for option in autocd globstar; do
    shopt -s "$option" 2> /dev/null;
done;

# Add tab completion for many Bash commands
if which brew &> /dev/null && [ -f "$(brew --prefix)/share/bash-completion/bash_completion" ]; then
    source "$(brew --prefix)/share/bash-completion/bash_completion";
elif [ -f /etc/bash_completion ]; then
    source /etc/bash_completion;
fi;

# Enable tab completion for `g` by marking it as an alias for `git`
if type _git &> /dev/null && [ -f /usr/local/etc/bash_completion.d/git-completion.bash ]; then
    complete -o default -o nospace -F _git g;
fi;

# Add tab completion for SSH hostnames based on ~/.ssh/config, ignoring wildcards
[ -e "$HOME/.ssh/config" ] && complete -o "default" -o "nospace" -W "$(grep "^Host" ~/.ssh/config | grep -v "[?*]" | cut -d " " -f2- | tr ' ' '\n')" scp sftp ssh;

# Add tab completion for `defaults read|write NSGlobalDomain`
# You could just use `-g` instead, but I like being explicit
complete -W "NSGlobalDomain" defaults;

# Add `killall` tab completion for common apps
complete -o "nospace" -W "Contacts Calendar Dock Finder Mail Safari iTunes SystemUIServer Terminal Twitter" killall;


